---
title: "Ark (R kernel) Di Zed"
date: 2024-09-17T19:03:45+07:00
categories: [tutorial]
tags: [rstats, linux]
---

[Zed](https://zed.dev/) merupakan angin segar bagi pasar IDE yang jenuh dengan VSCode. Seperti [Ladybird](https://ladybird.org/) dan [Zen Browser](https://zen-browser.app/) yang hadir di tengah-tengah berbagai macam peramban yang semua inti mesinnya lagi-lagi pakai Chromium.

Sebagai alat kerja baru, masih banyak ruang dan kesempatan bagi Zed untuk berkembang. Misalnya, untuk mendapatkan pengalaman menjalankan baris-per-baris kode seperti pada Jupyter Notebook atau RStudio dengan shortcut `Ctrl + Enter`.

Kita bisa menggunakan fitur REPL (Read-Eval-Print-Loop) di Zed untuk mendapatkan pengalaman kerja yang serupa dengan itu.

Posit (dulunya RStudio) mengembangkan suatu IDE suksesor dari pendahulunya RStudio. Namanya futuristik sekali: Positron. Lagi-lagi, ini merupakan "custom VSCode".

Untuk mengakomodir fitur-fitur yang sudah matang di RStudio, Posit mengembangkan beberapa engine untuk diintegrasikan di Positron. Salah satunya [Ark](https://github.com/posit-dev/ark), suatu kernel R untuk Jupyter berbasis bahasa Rust.

Ark tidak hanya berfokus untuk itu, tetapi juga ingin mengakomodir fitur antarmuka lainnya, multi-purpose, agar bisa digunakan juga untuk LSP (autocompletion dan hint saat mengetik kode) dan lain sebagainnya.

## Ark sebagai kernel REPL di Zed (Unix-like)

Ark merupakan salah satu kernel REPL yang didukung oleh ZED sebagaimana disebutkan di [dokumentasi](https://zed.dev/docs/repl#r-ark) resmi Zed. Tetapi kita perlu melakukan instalasi dan konfigurasi sendiri.

Hal pertama yang diperlukan tentu saja Zed yang sudah siap dan terinstal dengan baik. Saya hanya akan fokus pada konfigurasi di mesin Linux, mungkin MacOS juga caranya serupa. Saya belum mencobanya di Windows.

Jika Zed sudah terpasang, kita akan memerlukan beberapa tahap:

1. Instalasi Ark di Linux
2. Konfigurasi pada pengaturan kernel Ark di Jupyter
3. Konfigurasi Ark di Zed

Untuk perhatian, ketika melakukan instalasi Ark di mesin Linux, saya tidak memiliki Jupyter yang terpasang di sistem utama. Tetapi saya sudah pernah melakukan pemasangan Jupyter di lingkungan kerja conda. Jadi, kemungkinan besar sudah tersedia berkas-berkas dan direktori milik Jupyter sebagai penyimpanan konfigurasi, tetapi tanpa Ark.

### Memasang Ark

Langkah pertama, mengunduh zip berisi berkas binary sesuai sistem operasi dari [repositori resmi Ark](https://github.com/posit-dev/ark/releases).

Kita bisa menyimpannya dimanapun yang kita ingat dan inginkan. Sebagai contoh saya simpan berkas binary ini di `/usr/local/ark/`. Zed merekomendasikan penyimpanan di `/usr/local/bin/` sedangkan pengembang Ark sendiri merekomendasikan penyimpanan di `/usr/local/bin/ark/`.

```bash
sudo apt-get install -y unzip
sudo unzip ark-x.x.xxx-linux-x64.zip -d /usr/local/ark/
sudo chown -R $USER:$USER /usr/local/ark/
/usr/local/ark/ark --install
```

Ganti nama berkas zip sesuai lokasi dan tempat berkas binary disimpan.

Setelah perintah install dijalankan akan muncul lokasi penyimpanan konfigurasi kernel Ark dengan nama `kernel.json`. Kita akan sedikit menambahkan baris perintah pada berkas tersebut.

Jika instalasi gagal dilakukan barangkali perlu dilakukan instalasi Jupyter pada sistem terlebih dahulu, lalu ulangi baris perintah terakhir untuk instalasi.

### Pengaturan kernel Ark di Jupyter

Buka berkas konfigurasi `kernel.json` untuk Ark. Ketika melakukan instalasi di komputer saya, berkas konfigurasi ini terletak di `/home/$USER/.local/share/jupyter/kernels/ark/`. Isinya kurang lebih seperti berikut:

```json
{
  "argv": [
    "/usr/local/ark/ark",
    "--connection_file",
    "{connection_file}",
    "--session-mode",
    "notebook",
  ],
  "display_name": "Ark R Kernel",
  "language": "R",
  "env": {
    "R_HOME": "/usr/lib/R",
    "LD_LIBRARY_PATH": "/usr/lib/R/lib"
  }
}
```

Kita hanya perlu menambahkan flag argumen `--no-capture-streams` di akhir. Hal ini diperlukan agar baris log tidak ikut disertakan ketika menjalankan REPL. Sehinga isi berkas `kernel.json` akan menjadi seperti berikut:

```json
{
  "argv": [
    "/usr/local/ark/ark",
    "--connection_file",
    "{connection_file}",
    "--session-mode",
    "notebook",
    "--no-capture-streams"
  ],
  "display_name": "Ark R Kernel",
  "language": "R",
  "env": {
    "R_HOME": "/usr/lib/R",
    "LD_LIBRARY_PATH": "/usr/lib/R/lib"
  }
}
```

### Konfigurasi Ark di Zed

Buka Zed, kemudian pergi ke menu Settings atau tekan `Ctrl + ,` untuk membuka berkas pengaturan Zed di `settings.json`.

Sertakan baris berikut:

```json
{
  // ...
  "jupyter": {
    "kernel_selections": {
      "r": "ark"
    }
  }
  // ...
}
```
Kemudian simpan dan restart REPL dengan menekan kombinasi `Ctrl + Shift + P` untuk memunculkan command, lalu ketikkan `repl: restart`. Jika pengaturan berhasil kita akan menemukan icon REPL di sudut kanan Pane berbaris bersama fitur Zed yang lain. Untuk memastikan kernel sudah terdeteksi, bisa juga dengan mengetikan perintah `repl: sessions` di kolom perintah Zed, atau restart aplikasi Zed.

Preview.

<script async src="https://telegram.org/js/telegram-widget.js?22" data-telegram-post="remoteworkingid/714" data-width="100%"></script>

Untuk diskusi dan pertanyaan silakan kontak saya melalui email `getdata[at]duck[dot]com` atau [Telegram](https://t.me/akherlan).

Sekian. Terima kasih.
