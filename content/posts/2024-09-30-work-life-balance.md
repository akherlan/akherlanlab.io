---
title: "Kesalahan Fatal Work Life Balance"
date: 2024-09-30T11:33:45+07:00
categories: [work]
tags: [productivity]
---

Dalam lingkup sosial, kita adalah orang yang paling tahu apa yang menjadi prioritas dalam hidup kita sendiri. Bekerja —mencari uang— adalah prioritas ketimbang keperluan keluarga pada hari-hari biasa. Tapi bisa jadi sebaliknya pada jam-jam tertentu setiap harinya atau setiap pekan.

Mengurus anak atau keperluan dengan orangtua bukan hal yang tidak lebih penting daripada rapat koordinasi peluncuran aplikasi versi terbaru. Termasuk juga kesehatan pribadi.

Tidak ada yang lebih bertanggungjawab terhadap keluarga kita ketimbang diri kita sendiri selaku orangtua atau anaknya. Orang lain tidak bertanggungjawab atas mereka.

Maka tugas kita adalah membuat tim dan teman kerja kita mengerti tanggung jawab yang sedang kita emban dalam hidup kita. Seperti mereka memahami tugas dan tanggung jawab kita dalam hal pekerjaan.

Tapi memang, bisa memilih kegiatan yang akan dikerjakan adalah suatu kemewahan. Sekaligus tanggung jawab. Tugas-tugas kerja juga merupakan tanggungjawab. Kita hanya perlu membuat prioritas yang bisa disesuaikan berdasar waktu yang kita jalani.

Elena, mengajukan keberatannya atas istilah "work-life balance" meskipun ia adalah seorang ibu dan pemimpin di perusahaan. Bukan karena tidak mengimbangi kehidupan pribadi dan kehidupan kerja. Justru karena ia sangat peduli.

Menurutnya, "work-life balance" adalah istilah yang terdengar salah, karena itu bermakna utopis. Pola pikir kita akan tergiring pada asumsi bahwa kehidupan pribadi dan kerja haruslah imbang dalam porsi 50-50. Itu tidak mungkin terjadi.

Ia mengajukan suatu ralat atas istilah ini: "work-life prioritization". Sebab kerja tentu harusnya memang menjadi prioritas dalam hidup kita karena menyangkut kepentingan orang banyak. Tapi itu tidak selamanya. Ada kalanya keluarga adalah prioritas tertinggi yang harus diutamakan dalam waktu-waktu tertentu. Saat itulah pekerjaan HARUS ditinggalkan sementara waktu dan orang-orang harus juga bisa memahaminya.

Pekerjaan tidak akan pernah usai sampai kapanpun. Penting bagi kita untuk membuat batasan dalam bekerja dan mengkomunikasikannya pada orang lain. Baik itu batasan dalam bentuk jam kerja, penyesuaiannya, atau yang lain. Karena tidak ada orang lain yang paling paham tentang apa yang menjadi tanggungjawab kita (yang harus diprioritaskan) dalam satu waktu, ketimbang kita sendiri.

Manusia adalah pemimpin bagi diri sendiri yang hidup dalam lini waktu. Oleh sebab itulah ia bertanggung jawab atas kegiatan dan waktu yang ia pergunakan. Kita harus punya passion dalam bekerja, tapi juga jangan abai dan menyepelekan waktu dengan keluarga dan diri sendiri.

Tulisan lengkap [Elena](https://www.elenaverna.com/p/thoughts-on-work-life-balance).

