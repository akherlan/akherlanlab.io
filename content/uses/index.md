---
title: "List of my uses"
date: 2024-06-27T11:01:43+07:00
lastmod: 2024-07-19T19:59:00+07:00
---

Below is a list of necessarily having to help my days.

## Languages

### As a human

- English (conversational, professional purpose, non-native, daily)

- Bahasa Indonesia (fluent, mother tongue, native, daily)

- Sundanese (fluent, local and cultural usage, native, daily)

- Javanese (conversational, local and cultural usage, non-native)

- Malay (basic, professional and cultural purpose, non-native)

### With machine

- Python

- R

- Bash

- JavaScript

- HTML-CSS

## Hardware

- Thinkpad X240

    - 500GB HDD (I know exactly, should be put SSD on there after upgraded)

    - 4GB RAM (I know, I know, I need to upgrade. I ALWAYS need htop)

- Mouse: Wireless Mouse (Have no brand, I've no idea what's text on the box. Made in China)

- Phone: Samsung M10

- Some server and cloud service subscriptions, sometimes nothing

## Software

### On my laptop

- XFCE Terminal as prompter

- Codium as editor

- Neovim as editor yet, I also integrate it with Codium

- SublimeText, another editor sometimes

- Edge, Brave, Firefox as browsers

- htop for monitoring and (system) task manager

- Upwork time tracker

- Gnome Clocks as alarm and reminder

- Obsidian for temporary journaling if Capacities' server is down. Sad.

#### **Online or browser-based**

- Capacities for second-brain, note-taking, journaling, and project management. They have email and Telegram integration.

- Notion for collaboration and project management

- Omnivore for consuming newsletters and RSS feeds

- [time.fyi](https://time.fyi/) for time sync with clients

### On my phone

- Olauncher, a perfect minimalist and focus-oriented Android launcher

- Gmail for mailing and updates. I'm not a social media person.

- Duckduckgo Brower for internet surfing

- Note-to-self, offline-first messaging-like style note-taking app for contemplation and "curhat"

- Samsung Clock as alarm and daily tasks alert

- Samsung Dictionary for translation

- Googel Maps for navigation

- WhatsApp Business for communication

- YouTube for entertainment and Indonesia news updates

### Both on phone and laptop

- Telegram Messanger for messaging, note-taking, reminder, sync tasks, entertainment, and almost all things you need to handle and keep sync with your laptop

- Bitwarden for password manager

- Raindrop for reading and learning



While listing this all stuffs, I realized that I surprisingly focus on reading, text editing, and note creation. It shapes me in better communicate with text and email rather than verbal. Moreover, I am a remote worker in which communication with people often happen indirectly or asyncronously.

If you consider to call me or let me in to your video call session, please take a while for a second thought and ask yourself "is it as that necessarily and urgently as?". If the answer "yes" still, go ahead to call me...... On a scheduled basis please.

